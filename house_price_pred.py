# python house_price_pred.py
import numpy as np
import pandas as pd
from sklearn.linear_model import LinearRegression


class AnalysisDataAndFitLinearRegression:

    def __init__(self):
        self.version = 1


    def predict(self, X):
        # Appending a cloumn of ones in X to add the bias term.
        X = np.append(X, np.ones((X.shape[0],1)), axis=1)
        # preds is y_hat which is the dot product of X and theta.
        preds = np.dot(X, self.theta)
        return preds
    
    def analyse_and_fit_lrm(self, path="./realest.csv"):
        # a path to a dataset is "./data/realest.csv"
        # dataset can be loaded by uncommenting the line bellow
        # data = pd.read_csv(path)
        data = pd.read_csv(path)
        tax_of_2bath_4bed = data[(data["Bathroom"]==2) & (data["Bedroom"]==4)]["Tax"]
        
        q_5 = data['Lot'].quantile(0.2)
        count = data[data['Lot'] >= q_5]['Lot'].count()

        output = {
            "summary_dict":{
                "statistics": [
                    tax_of_2bath_4bed.mean(),
                    tax_of_2bath_4bed.std(),
                    tax_of_2bath_4bed.median(),
                    tax_of_2bath_4bed.min(),
                    tax_of_2bath_4bed.max(),
                    ],
                "data_frame": data[data["Space"]>800].sort_values(by=["Price"], ascending=False),
                "number_of_observations": count
            },
            "regression_dict": {}
        }

        data = self.__listwise_deletion(data)
        X = data.drop(columns=["Price"])
        X_col = X.columns.tolist()
        y = data["Price"]

        X = X.values
        y = y.values

        m = X.shape[0] # Number of training examples. 
        # Appending a cloumn of ones in X to add the bias term.
        X = np.append(X, np.ones((m,1)), axis=1)    
        # reshaping y to (m,1)
        y = y.reshape(m,1)
    
        # The Normal Equation
        self.theta = np.dot(np.linalg.inv(np.dot(X.T, X)), np.dot(X.T, y))
        theta = self.theta.reshape(-1)
        X_col.extend(["Intercept"])
        
        
        output["regression_dict"]["model_parameters"] = dict(zip(X_col, theta))
        
        
    #     Index(['Bedroom', 'Space', 'Room', 'Lot', 'Tax', 'Bathroom', 'Garage',
    #    'Condition'],
        ip = np.array([3, 1500, 8, 40, 1000, 2, 1, 0])
        ip = ip.reshape(1, -1)

        output["regression_dict"]["price_prediction"] = self.predict(ip)[0][0]

        return output
        
           


        

    def __listwise_deletion(self, data: pd.DataFrame):
        return data.dropna()

if __name__ == "__main__":
    obj = AnalysisDataAndFitLinearRegression()
    op = obj.analyse_and_fit_lrm()
    from pprint import pprint
    pprint(op)
