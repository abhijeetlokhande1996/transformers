# we are using gpt-2 model
# and using beam search approach

import torch
from transformers import AutoTokenizer, AutoModelForCausalLM

device = "cuda" if torch.cuda.is_available() else "cpu"
model_name = "gpt2-xl"
tokenizer = AutoTokenizer.from_pretrained(model_name)
model = AutoModelForCausalLM.from_pretrained(model_name).to(device)

max_length = 128
input_txt = """Transformers are the most popular toy line in the world"""

model_input = tokenizer(input_txt, return_tensors="pt")
# model input contain attention mask, token embedding
output_beam = model.generate(**model_input, max_length=max_length, num_beams=5, do_sample=True)

output = tokenizer.decode(output_beam[0])
print(output)

