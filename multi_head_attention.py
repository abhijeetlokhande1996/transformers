"""
    before multi head attention

    text -> token endocing -> token embedding -> multi head attention

    input to the attention head is token embedding

"""
import torch
from transformers import AutoTokenizer
from torch import nn
from transformers import AutoConfig
from math import sqrt
import torch.nn.functional as F

def scaled_dot_product_attention(query, key, value):
    dim_k = query.size(-1)
    attn_scores = torch.bmm(query, key.transpose(1, 2)) / sqrt(dim_k)
    attn_scores = F.softmax(attn_scores, dim=-1)
    attn_weights = torch.bmm(attn_scores, value)
    return attn_weights

class AttentionHead(nn.Module):
    # this is single attention head
    # one single attention head contain query, key and value
    # and scaled dot product
    def __init__(self, embed_dim, head_dim) -> None:
        super().__init__()
        self.q = nn.Linear(embed_dim, head_dim)
        self.k = nn.Linear(embed_dim, head_dim)
        self.v = nn.Linear(embed_dim, head_dim)
    
    def forward(self, hidden_state) -> None:
        return scaled_dot_product_attention(
            self.q(hidden_state), self.k(hidden_state), self.v(hidden_state))

class MultiHeadAttention(nn.Module):
    def __init__(self, config) -> None:
        super().__init__()
        embed_dim = config.hidden_size
        num_head = config.num_attention_heads
        head_dim = embed_dim // num_head
        self.heads = nn.ModuleList([AttentionHead(embed_dim, head_dim) for _ in range(num_head)])

        self.output_linear = nn.Linear(embed_dim, embed_dim)
    
    def forward(self, hidden_state):
        x = torch.cat([head(hidden_state) for head in self.heads], dim=-1)
        x = self.output_linear(x)
        return x



if __name__ == "__main__":
    
    model_ckpt = "bert-base-uncased"
    text = "time flies like an arrow"
    tokenizer = AutoTokenizer.from_pretrained(model_ckpt)

    inputs = tokenizer(text, return_tensors="pt", add_special_tokens=False)

    config = AutoConfig.from_pretrained(model_ckpt)
    print(config)

    token_emb = nn.Embedding(config.vocab_size, config.hidden_size)
    input_embeds = token_emb(inputs.input_ids)

    # query = key = value = input_embeds
    # dim_k = input_embeds.size(-1)

    # attn_scores = torch.bmm(query, key.transpose(1, 2)) / sqrt(dim_k)
    # attn_weights = F.softmax(attn_scores, dim=-1)

    # attn_outputs = torch.bmm(attn_weights, value)
    
    multihead_attn = MultiHeadAttention(config)
    multihead_attn(input_embeds)


    # masked attention
    seq_length = inputs.input_ids.size(-1)
    mask = torch.tril(torch.ones(seq_length, seq_length)).unsqueeze(0)
    print(mask[0])
    



    


