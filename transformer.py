"""
    token encoding -> token embedding -> multi head attention -> feed forward neural network 
        -> hidden state(or ouput of an ecoder)
    above token embedding is not normal embedding, it contains normal embedding as well as positional embedding
"""

import torch
from transformers import AutoTokenizer
from torch import nn
from transformers import AutoConfig
from math import sqrt
import torch.nn.functional as F

def scaled_dot_product_attention(query, key, value):
    dim_k = query.size(-1)
    attn_scores = torch.bmm(query, key.transpose(1, 2)) / sqrt(dim_k)
    attn_scores = F.softmax(attn_scores, dim=-1)
    attn_weights = torch.bmm(attn_scores, value)
    return attn_weights

class AttentionHead(nn.Module):
    # this is single attention head
    # one single attention head contain query, key and value
    # and scaled dot product
    def __init__(self, embed_dim, head_dim) -> None:
        super().__init__()
        self.q = nn.Linear(embed_dim, head_dim)
        self.k = nn.Linear(embed_dim, head_dim)
        self.v = nn.Linear(embed_dim, head_dim)
    
    def forward(self, hidden_state) -> None:
        return scaled_dot_product_attention(
            self.q(hidden_state), self.k(hidden_state), self.v(hidden_state))

class MultiHeadAttention(nn.Module):
    def __init__(self, config) -> None:
        super().__init__()
        embed_dim = config.hidden_size
        num_head = config.num_attention_heads
        head_dim = embed_dim // num_head
        self.heads = nn.ModuleList([AttentionHead(embed_dim, head_dim) for _ in range(num_head)])

        self.output_linear = nn.Linear(embed_dim, embed_dim)
    
    def forward(self, hidden_state):
        x = torch.cat([head(hidden_state) for head in self.heads], dim=-1)
        x = self.output_linear(x)
        return x

class FeedForward(nn.Module):
    def __init__(self, config) -> None:
        super().__init__()
        self.linear1 = nn.Linear(config.hidden_size, config.intermediate_size)
        self.linear2 = nn.Linear(config.intermediate_size, config.hidden_size)
        self.gelu = nn.GELU()
        self.dropout = nn.Dropout(config.hidden_dropout_prob)
    
    def forward(self, x):
        x = self.linear1(x)
        x = self.gelu(x)
        x = self.linear2(x)
        x = self.dropout(x)
        return x

class TransformerEncoderLayer(nn.Module):
    """
        this class contain whole process of transformer encoder
        input to this is output of token embedding
        after getting token embedding 
        1. we will normalise it using layer norm to avoid unstable training and warming learning rate
        2. after layernorm we will go to Multi Head Attention
        3. we will add token embedding to output of multi head attn
        4. we normalise output of step 3 and give it to the feed forward layers
    """
    def __init__(self, config) -> None:
        super().__init__()
        self.layer_norm_1 = nn.LayerNorm(config.hidden_size)
        self.layer_norm_2 = nn.LayerNorm(config.hidden_size)
        self.attention = MultiHeadAttention(config)
        self.feed_forward = FeedForward(config)
    
    def forward(self, x):
        hidden_state = self.layer_norm_1(x)
        x = x + self.attention(hidden_state)
        x = x + self.feed_forward(self.layer_norm_2(x))
        return x


class Embeddings(nn.Module):
    def __init__(self, config) -> None:
        super().__init__()

        self.token_embeddings = nn.Embedding(config.vocab_size, config.hidden_size)
        self.positional_embeddings = nn.Embedding(config.max_position_embeddings, config.hidden_size)
        self.layer_norm = nn.LayerNorm(config.hidden_size, eps=1e-12)
        self.dropout = nn.Dropout()
    
    def forward(self, input_ids):
        seq_length = input_ids.size(-1)
        position_ids = torch.arange(seq_length, dtype=torch.long).unsqueeze(0)
        token_embeddings = self.token_embeddings(input_ids)
        position_embeddings = self.positional_embeddings(position_ids)
        embeddings = token_embeddings + position_embeddings
        embeddings = self.layer_norm(embeddings)
        embeddings = self.dropout(embeddings)
        return embeddings

class TransformerEncoder(nn.Module):
    def __init__(self, config) -> None:
        super().__init__()
        self.embeddings = Embeddings(config)
        self.layers = [TransformerEncoderLayer(config) for _ in range(config.num_hidden_layers)]
    
    def forward(self, x):
        x = self.embeddings(x)
        for layer in self.layers:
            x = layer(x)
        return x

    
if __name__ == "__main__":
    model_ckpt = "bert-base-uncased"
    config = AutoConfig.from_pretrained(model_ckpt)
    text = "time flies like an arrow"

    tokenizer = AutoTokenizer.from_pretrained(model_ckpt)
    inputs = tokenizer(text, return_tensors="pt", add_special_tokens=False)
    token_emb = nn.Embedding(config.vocab_size, config.hidden_size)
    input_embeds = token_emb(inputs.input_ids)

    multihead_attn = MultiHeadAttention(config)
    attn_outputs = multihead_attn(input_embeds)
    feed_forward = FeedForward(config)
    ff_outputs = feed_forward(attn_outputs)

    encoder_layer = TransformerEncoderLayer(config)
    x = encoder_layer(input_embeds)

    embedding = Embeddings(config)
    x = embedding(inputs.input_ids)

    enc = TransformerEncoder(config)
    x = enc(inputs.input_ids)
    print(x.size())

    


