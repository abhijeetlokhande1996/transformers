from transformers import M2M100ForConditionalGeneration, M2M100Tokenizer

eng_text = """India has the second-largest population in the world. 
    India is also knowns as Bharat, Hindustan and sometimes Aryavart. 
    It is surrounded by oceans from three sides which are Bay Of Bengal in the east, 
    the Arabian Sea in the west and Indian oceans in the south.
"""


model = M2M100ForConditionalGeneration.from_pretrained("facebook/m2m100_418M")
tokenizer = M2M100Tokenizer.from_pretrained("facebook/m2m100_418M")

# translate Hindi to French
tokenizer.src_lang = "en"
encoded_hi = tokenizer(eng_text, return_tensors="pt")
generated_tokens = model.generate(**encoded_hi, forced_bos_token_id=tokenizer.get_lang_id("de"))
resp = tokenizer.batch_decode(generated_tokens, skip_special_tokens=True)
print(resp)

