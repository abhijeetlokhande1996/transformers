from collections import defaultdict
from  transformers import AutoTokenizer

MODEL_CHECKPOINT = "bert-base-cased"
TOKENIZER = AutoTokenizer.from_pretrained(MODEL_CHECKPOINT)


def compute_pair_scores(splits, word_freqs):
    # compute pair score
    # for example score(##gs) = freq(##gs)/(freq(##g) * freq(##s))

    letter_freqs = defaultdict(int)
    pair_freqs = defaultdict(int)
    
    for word, freq in word_freqs.items():
        split = splits[word]
        
        if len(split) == 1:
            letter_freqs[split[0]]+=freq
            continue
        for i in range(len(split)-1):
            pair = (split[i], split[i+1])
            pair_freqs[pair]+=freq
            letter_freqs[split[i]]+=freq
        letter_freqs[split[-1]]+=freq

    scores = {
        pair: freq / (letter_freqs[pair[0]] * letter_freqs[pair[1]])
        for pair, freq in pair_freqs.items()
        }    
    return scores    

def merge_pair(a, b, splits, word_freqs):
    for word in word_freqs:
        split = splits[word]
        if len(split)==1:
            continue
        i=0
        while i<len(split)-1:
            if split[i]==a and split[i+1]==b:
                merge = a+b[2:] if b.startswith("##") else a+b
                split = split[:i]+[merge]+split[i+2:]
            else:
                i+=1
        splits[word] = split
    return splits

def word_piece_tokenization(corpus, tokenizer=TOKENIZER):
    # first we compute the frequencies of each word in the corpus
    word_freqs = defaultdict(int)

    for text in corpus:
        words_with_offsets = tokenizer.backend_tokenizer.pre_tokenizer.pre_tokenize_str(text)
        new_words = [word for word, _ in words_with_offsets]
        for word in new_words:
            word_freqs[word]+=1
    
    # now compute alphabet
    # alphabet is the unique set composed of all first letters of words and all the 
    # other letters that appear in words prefixed by ##

    alphabet = []
    for word in word_freqs.keys():
        if word[0] not in alphabet:
            alphabet.append(word[0])
        for letter in word[1:]:
            if f"##{letter}" not in alphabet:
                alphabet.append(f"##{letter}")
    
    alphabet.sort()
    
    # now it's time to create vocab
    # and add "pad, unk, cls, sep and mask"
    vocab = ["[PAD]", "[UNK]", "[CLS]", "[SEP]", "[MASK]"] + alphabet.copy()
    
    # split each word
    # and we only need to split the which is not starting ##

    splits = {
        word: [c if i==0 else f"##{c}" for i,c in enumerate(word)]
        for word in word_freqs.keys()
    }
    

    pair_scores = compute_pair_scores(splits, word_freqs)

    # let's look at pair scores
    for i, key in enumerate(pair_scores.keys()):
        print(f"{key}: {pair_scores[key]}")
        if i > 5:
            break
    
    # now, finding the pair with best score

    best_pair = ""
    max_score = None

    for pair, score in pair_scores.items():
        if max_score is None or max_score < score:
            max_score = score
            best_pair = pair
    print("--"*10)
    print(best_pair, max_score)

    splits = merge_pair("a", "##b", splits, word_freqs)
    print(splits["about"])

    vocab_size = 70

    print("--"*10)
    while len(vocab) < vocab_size:
        scores = compute_pair_scores(splits, word_freqs)
        best_pair, max_score = "", None

        for pair, score in scores.items():
            if max_score is None or max_score<score:
                max_score=score
                best_pair=pair
        
        splits = merge_pair(a=best_pair[0], b=best_pair[1], splits=splits, word_freqs=word_freqs)

        new_token = (
            best_pair[0]+best_pair[1][2:]
            if best_pair[1].startswith("##")
            else best_pair[0]+best_pair[1]
        )

        vocab.append(new_token)
    
    
    return vocab

    






        

def encode_word(word, vocab):
    tokens = []
    
    while len(word)>0:
        i = len(word)

        while i>0 and word[:i] not in vocab:
            i-=1
        if i==0:
            return ["[UNK]"]
        tokens.append(word[:i])
        word = word[i:]

        if len(word)>0:
            word = f"##{word}"


    return tokens

def tokenize(text, vocab, tokenizer=TOKENIZER):
    pre_tokenize_result = tokenizer._tokenizer.pre_tokenizer.pre_tokenize_str(text)
    pre_tokenized_text = [word for word, offset in pre_tokenize_result]
    encoded_words = [encode_word(word, vocab) for word in pre_tokenized_text]
    return sum(encoded_words, [])

if __name__ == "__main__":
    my_corpus = [
        "This is the Hugging Face Course.",
        "This chapter is about tokenization.",
        "This section shows several tokenizer algorithms.",
        "Hopefully, you will be able to understand how they are trained and generate tokens."]
    
    vocab = word_piece_tokenization(my_corpus, tokenizer=TOKENIZER)
    print(encode_word("HOgging", vocab))
    op = tokenize("This is the Hugging Face course!", vocab)
    print(op)

